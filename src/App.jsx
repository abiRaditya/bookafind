import HomeScreen from "./screens/HomeScreen";
import Bookmarks from "./screens/Bookmarks";
import "./App.scss";
import {
  Routes,
  Route,
  // Link,
} from "react-router-dom";

import "sweetalert2/src/sweetalert2.scss";

function App() {
  return (
    <Routes>
      <Route path="/bookmarks" element={<Bookmarks />} />
      <Route path="/" element={<HomeScreen />} />
    </Routes>
  );
}

export default App;
