import React, { useEffect, useRef } from "react";
import BookCards from "../components/BookCards";
import bookmarksApi from "../repositories/bookmarksApi";
import Swal from "sweetalert2";
import { getBookmarksThunk } from "./features/bookMark";

import { useSelector, useDispatch } from "react-redux";

const Bookmarks = () => {
  const dispatch = useDispatch();
  const bookmarks = useSelector((state) => state.bookmark.bookmarks);
  const isBookmarkLoading = useSelector(
    (state) => state.bookmark.isBookmarkLoading
  );

  function useCancelToken() {
    const token = useRef(new AbortController());
    const signal = () => token.current.signal;
    return [token.current, signal];
  }

  const [token, signal] = useCancelToken();

  async function starsOnChange(starValue, id) {
    //
    try {
      Swal.showLoading();
      const response = await bookmarksApi.patchStarBookmarks({
        starValue,
        id,
      });
      Swal.hideLoading();
      if (response?.response?.status === 404) {
        throw response.response?.data.message;
      }
      // dispatch(getBookmarksThunk());
      Swal.fire({
        title: "Success",
        text: `Rating updated`,
        icon: "success",
      });
    } catch (error) {
      console.log(error, "starsOnChange");
      Swal.hideLoading();
      dispatch(getBookmarksThunk({ signal, token }));
      await Swal.fire({
        title: "Error!",
        text: error,
        icon: "error",
      });
    }
  }

  async function removeBookmark(bookId) {
    try {
      Swal.showLoading();
      const response = await bookmarksApi.delBookmarks(bookId);
      if (response?.response?.status === 404) {
        throw response.response?.data.message;
      }
      Swal.hideLoading();
      Swal.fire({
        title: "Success",
        text: `Bookmark removed`,
        icon: "success",
      });
      dispatch(getBookmarksThunk({ signal, token }));
    } catch (error) {
      console.log(error, "getBookmarks");
      await Swal.fire({
        title: "Error!",
        text: error,
        icon: "error",
      });
    }
  }
  useEffect(() => {
    console.log(`use effect getBookmarksThunk`);
    dispatch(getBookmarksThunk({ signal, token })); // eslint-disable-next-line
    // eslint-disable-next-line
  }, []); // eslint-disable-next-line
  return (
    <div className="bookmarks-page">
      <div className="bookafind-header">
        <h1>List of your bookmarks</h1>
        <p>Search and bookmark your favorite books</p>
      </div>

      <div className="bookcards-container">
        {isBookmarkLoading ? (
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        ) : bookmarks.length ? (
          bookmarks.map((book) => {
            return (
              <BookCards
                authors={book.authors}
                image={book.imageLink}
                title={book.title}
                bookId={book.id}
                rating={book.ratingsCount}
                isRemove={true}
                removeFunc={removeBookmark}
                key={book.id}
                starsOnChange={starsOnChange}
              />
            );
          })
        ) : (
          <div className="empty-bookmarks">Empty</div>
        )}
      </div>
    </div>
  );
};

export default Bookmarks;
