import React from "react";

const HomeScreen = () => {
  return (
    <div>
      <div className="bookafind-header">
        <h1>Welcome to BookAfind</h1>
        <p>Search and bookmark your favorite books</p>
      </div>
      <div className="rounded-image">
        <img src={require("../assets/books.jfif")} alt="" />
      </div>
      <div className="bookafind-quote">
        <h1>
          “The more that you read, the more things you will know. The more that
          you learn, the more places you'll go.”
        </h1>
        <p>
          Dr. Seuss (American children's author, political cartoonist,animator,
          and filmmaker)
        </p>
      </div>
    </div>
  );
};

export default HomeScreen;
