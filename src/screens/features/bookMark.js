import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import bookmarksApi from "../../repositories/bookmarksApi";

export const getBookmarksThunk = createAsyncThunk(
  `bookmark/getAll`,
  async ({ signal, token }, { rejectWithValue }) => {
    try {
      const response = await bookmarksApi.getBookmarks({ signal: signal() });
      // token.abort();
      //   thunkAPI
      console.log(response, "getBookmarksThunk resposne data ");
      return response;
    } catch (error) {
      console.log(error, "createAsyncThunk getBookmarks");
      rejectWithValue(error);
    }
  }
);
const initialState = {
  isBookmarkLoading: false,
  bookmarks: [],
};
export const bookMarkSlice = createSlice({
  name: "bookmark",
  initialState,
  reducers: {},
  extraReducers: {
    [getBookmarksThunk.pending]: (state) => {
      state.isBookmarkLoading = true;
    },
    [getBookmarksThunk.fulfilled]: (state, { payload }) => {
      state.isBookmarkLoading = false;
      state.bookmarks = payload;
    },
    [getBookmarksThunk.rejected]: (state) => {
      state.isBookmarkLoading = false;
    },
  },
  //    (builder) => {
  //     builder.addCase(getBookmarks.fulfilled, (state, action) => {
  //       state.bookmarks = action.payload;
  //     });
  //   },
});
// export const bookmarkReducer = bookMarkSlice.reducer;
export default bookMarkSlice.reducer;
