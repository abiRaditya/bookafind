import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isModalOpen: false,
  isSearchLoading: false,
  books: [],
};

export const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    setIsModalOpen: (state, action) => {
      state.isModalOpen = action.payload;
    },
    setIsSearchLoading: (state, action) => {
      state.isSearchLoading = action.payload;
    },
    setBooks: (state, action) => {
      state.books = action.payload;
    },
  },
});

export const { setIsModalOpen, setIsSearchLoading, setBooks } =
  modalSlice.actions;

export default modalSlice.reducer;
