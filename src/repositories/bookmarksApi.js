import axios from "axios";
const config = {
  baseURL: `https://bookafind-production.up.railway.app/favorites`,
};
export default class bookmarksApi {
  static async postBookmarks({ payload }) {
    try {
      const response = await axios({
        ...config,
        method: `post`,
        data: payload,
      });
      return response.data;
    } catch (error) {
      console.log(error, "postBookmarks error");
      return error;
    }
  }
  static async getBookmarks() {
    try {
      const response = await axios({ ...config, method: `get` });
      return response.data;
    } catch (error) {
      console.log(error, "getBookmarks error");
      return error;
    }
  }
  static async delBookmarks(id) {
    try {
      const response = await axios({
        ...config,
        method: `delete`,
        url: `/${id}`,
      });
      return response.data;
    } catch (error) {
      console.log(error, "getBookmarks error");
      return error;
    }
  }
  static async patchStarBookmarks({ starValue, id }) {
    try {
      const response = await axios({
        ...config,
        data: {
          ratingsCount: starValue,
        },
        method: `patch`,
        url: `/${id}`,
      });
      return response.data;
    } catch (error) {
      console.log(error, "getBookmarks error");
      return error;
    }
  }
}
