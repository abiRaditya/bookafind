import AxiosInstance from ".";
export default class SearchApi {
  static async search(search) {
    try {
      let params = new URLSearchParams({ q: search });
      const response = await AxiosInstance({
        method: "get",
        params: params,
      });
      return response.data;
    } catch (error) {
      console.log(error, "error search");
    }
  }
}
