import ReactStars from "react-rating-stars-component";
import React, { useRef } from "react";

const BookCards = ({
  title,
  authors,
  image,
  bookId,
  submitFunc,
  removeFunc,
  rating,
  isRemove,
  starsOnChange,
}) => {
  const ratingRef = useRef();
  const starsConfig = {
    size: 24,
    count: 5,
    value: rating ? rating : 0,
    isHalf: true,
    onChange: starsOnChange
      ? (newRating) => {
          starsOnChange(newRating, bookId);
        }
      : (newRating) => {
          ratingRef.current = newRating;
        },
  };
  function submitBookmark() {
    const payload = {
      title,
      authors: authors,
      imageLink: image,
      id: bookId,
      ratingsCount: ratingRef.current ? ratingRef.current : 0,
    };
    console.log(payload, "bookmark hit api");
    submitFunc(payload);
  }
  function removeBookmark() {
    removeFunc(bookId);
  }
  function buttonHandler() {
    if (isRemove) {
      removeBookmark(bookId);
    } else {
      submitBookmark();
    }
  }
  return (
    <div className="book-cards">
      <div className="book-cover">
        <img src={image} alt="" />
      </div>
      <div className="book-information">
        <div>
          <h3>{title}</h3>
          <p>By {authors ? (authors[0] ? authors[0] : "-") : ""}</p>
          <ReactStars {...starsConfig} />
        </div>
        <div className={isRemove ? "remove-button" : "bookmark-button"}>
          <button onClick={buttonHandler}>
            {isRemove ? "Remove" : "Bookmark"}
          </button>
        </div>
        {/* onStarClick={this.onStarClick.bind(this)} */}
      </div>
    </div>
  );
};

export default BookCards;
