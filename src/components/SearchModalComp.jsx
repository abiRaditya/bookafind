import React, { useRef } from "react";
import BookCards from "./BookCards";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import bookmarksApi from "../repositories/bookmarksApi";
import Swal from "sweetalert2";
import { setIsModalOpen } from "../screens/features/modalSlice";
import { getBookmarksThunk } from "../screens/features/bookMark";

const SearchModalComp = () => {
  const dispatch = useDispatch();
  const books = useSelector((state) => state.modal.books);
  let location = useLocation();

  function useCancelToken() {
    const token = useRef(new AbortController());
    const signal = () => token.current.signal;
    return [token.current, signal];
  }

  const [token, signal] = useCancelToken();

  async function submitBookmark(payload) {
    try {
      // token.abort();
      const isFetchBookmarks = location.pathname === "/bookmarks";
      Swal.showLoading();
      const response = await bookmarksApi.postBookmarks({ payload, signal });
      Swal.hideLoading();
      if (response?.response?.status === 400) {
        throw response?.response?.data?.message;
      }
      await Swal.fire({
        title: "Saved",
        text: "Bookmark saved",
        icon: "success",
        confirmButtonText: "Cool",
      });
      if (isFetchBookmarks) {
        dispatch(getBookmarksThunk({ signal, token }));
      }
      dispatch(setIsModalOpen(false));
    } catch (error) {
      Swal.hideLoading();
      console.log(error, "submitBookmark error");
      await Swal.fire({
        title: "Error!",
        text: error,
        icon: "error",
      });
    }
  }
  return (
    <div className="search-results">
      {books.map((book) => {
        return (
          <BookCards
            key={book.id}
            authors={book.volumeInfo.authors}
            image={book.volumeInfo.imageLinks?.thumbnail}
            title={book.volumeInfo.title}
            bookId={book.id}
            submitFunc={submitBookmark}
          />
        );
      })}
    </div>
  );
};

export default SearchModalComp;
