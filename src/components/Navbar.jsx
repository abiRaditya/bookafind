import React, { useMemo, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import SearchApi from "../repositories/searchBooksApi";
import ModalService from "../service/ModalService";
import SearchModalComp from "./SearchModalComp";

import {
  setIsModalOpen,
  setIsSearchLoading,
  setBooks,
} from "../screens/features/modalSlice";
import debounce from "lodash.debounce";

const Navbar = () => {
  const isModalOpen = useSelector((state) => state.modal.isModalOpen);
  const isSearchLoading = useSelector((state) => state.modal.isSearchLoading);
  const searchRef = useRef();
  const dispatch = useDispatch();

  function closeModal() {
    dispatch(setIsModalOpen(false));
  }
  function openModal() {
    dispatch(setIsSearchLoading(false));
    searchRef.current.value = "";
    dispatch(setIsModalOpen(true));
  }

  const handleChange = async (e) => {
    dispatch(setIsSearchLoading(true));
    try {
      if (!e.target.value) {
        dispatch(setIsSearchLoading(false));
        return;
      }
      const data = await SearchApi.search(e.target.value);
      dispatch(setBooks(data.items));
      openModal();
    } catch (error) {
      console.log(error, "error");
      dispatch(setIsSearchLoading(false));
      closeModal();
    }
  };
  const debouncedResults = useMemo(() => {
    return debounce(handleChange, 1000);
  }, []); // eslint-disable-line
  function componentFunction(Component) {
    return (props) => <Component {...props} />;
  }
  const ModalSearchResult = componentFunction(ModalService);
  return (
    <nav className="navbar" id="navigation-bar">
      <ModalSearchResult
        isModalOpen={isModalOpen}
        closeModal={closeModal}
        modalTitle={"Search Results"}
      >
        <SearchModalComp />
      </ModalSearchResult>
      <NavLink to="/">Home</NavLink>
      <div className="search">
        <input
          type="search"
          name=""
          placeholder="Search"
          id=""
          onChange={debouncedResults}
          ref={searchRef}
          disabled={isSearchLoading}
        />
        {isSearchLoading ? (
          <div className="loading-container">
            <div className="loading-infinite"></div>
          </div>
        ) : null}
      </div>

      <NavLink to="/bookmarks">Bookmarks</NavLink>
    </nav>
  );
};

export default Navbar;
