import { configureStore } from "@reduxjs/toolkit";
import modalSlice from "./screens/features/modalSlice";
import bookMarkSlice from "./screens/features/bookMark";

export const store = configureStore({
  reducer: {
    modal: modalSlice,
    bookmark: bookMarkSlice,
  },
});
